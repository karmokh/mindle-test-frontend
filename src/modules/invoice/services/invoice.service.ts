import {IInvoice} from "../models/invoice.model";
import {AxiosError, AxiosResponse} from "axios";
import {IFindAllServiceParams} from "../../../types/request-types";
import axios from "../../../axios/axios";

export function findAllInvoicesService({page, limit, cb, errorHandler}: IFindAllServiceParams) {
    axios.get('/invoices',
        {
            params: {
                page,
                limit,
            }
        }
    )
        .then(response => {
            cb && cb(response.data)
        })
        .catch(err => {
            errorHandler && errorHandler(err.response.data)
        })
}

export function findOneInvoiceService({id, cb, errorHandler}: {
    id: string,
    cb: (res: AxiosResponse) => void,
    errorHandler: (err: AxiosError) => void
}) {
    axios.get(`/invoices/${id}`)
        .then(response => {
            cb(response.data)
        })
        .catch(err => {
            errorHandler(err.response.data)
        })
}

export function createInvoiceService(values: IInvoice, cb: (res: AxiosResponse) => void, errorHandler: (err: AxiosError) => void) {
    axios.post('/invoices', values)
        .then(response => {
            cb(response.data)
        })
        .catch(err => {
            errorHandler(err.response.data)
        })
}

export function updateInvoiceService(id: string, values: IInvoice, cb: (res: AxiosResponse) => void, errorHandler: (err: AxiosError) => void) {
    axios.patch(`/invoices/${id}`, values)
        .then(response => {
            cb(response.data)
        })
        .catch(err => {
            errorHandler(err.response.data)
        })
}

export function deleteInvoiceService({id, cb, errorHandler}: {
    id: string,
    cb: (res: AxiosResponse) => void,
    errorHandler: (err: AxiosError) => void
}) {
    axios.delete(`/invoices/${id}`)
        .then(response => {
            cb(response.data)
        })
        .catch(err => {
            errorHandler(err.response.data)
        })
}

export function deleteMultipleInvoicesService({id, cb, errorHandler}: {
    id: string[],
    cb: (res: AxiosResponse) => void,
    errorHandler: (err: AxiosError) => void
}) {
    axios.delete('/invoices/multiple', {
        data: id
    })
        .then(response => {
            cb(response.data)
        })
        .catch(err => {
            errorHandler(err.response.data)
        })
}