import React, {Dispatch, SetStateAction} from "react";
import {Switch} from "@nextui-org/react";
import {Moon, Sun} from "react-feather";
import {useTranslation} from 'react-i18next';

function Header({setTheme}: { setTheme: Dispatch<SetStateAction<string>> }) {
    const {t, i18n} = useTranslation();

    return (
        <div
            className="fixed flex w-screen top-0 left-0 bg-default-background h-12 z-10 drop-shadow-3xl p-5 items-center justify-between border-default-foreground"
            style={{borderBottom: "1px solid"}}>
            <div className={"flex flex-row items-center"}>
                <span className={"text-blue text-sm"}>{t('app-name')}</span>
            </div>
            <div className={"flex flex-row items-center"}>
                <Switch
                    defaultSelected
                    onValueChange={(val) => {
                        setTheme(val ? 'light' : 'dark')
                    }}
                    size="md"
                    color="primary"
                    startContent={<Sun/>}
                    endContent={<Moon/>}
                ></Switch>
            </div>
        </div>
    );
}

export default Header;
