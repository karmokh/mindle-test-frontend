import {AxiosError, AxiosResponse} from "axios";

export interface IFindAllServiceParams {
    page?: number,
    limit?: number,
    cb?: (res: AxiosResponse) => void,
    errorHandler?: (err: AxiosError) => void
}

export interface IColumn {
    uid: string;
    name: string;
    render: (item: any, column: string) => JSX.Element;
}