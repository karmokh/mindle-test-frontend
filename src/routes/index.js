import React from "react";
import {Routes} from "react-router-dom";
import {PersonRoutes} from "../modules/person";
import {ProductRoutes} from "../modules/product";
import {InvoiceRoutes} from "../modules/invoice";

const Router = () => {
    const modules = [
        PersonRoutes,
        ProductRoutes,
        InvoiceRoutes
    ]
    const renderModules = () => modules.map(m => m.map(route => route))

    return (<Routes>{renderModules()}</Routes>);
};

export default React.memo(Router);
