import {PDFDocument, StandardFonts} from "pdf-lib";
import {IInvoice} from "../modules/invoice/models/invoice.model";

export const invoicePdfGenerate = async (invoice: IInvoice) => {
    const pdfDoc = await PDFDocument.create()
    const timesRomanFont = await pdfDoc.embedFont(StandardFonts.TimesRoman)

    const page = pdfDoc.addPage()
    const {width, height} = page.getSize()
    const fontSize = 14
    let yController = 2
    page.drawText('Sale Invoice', {
        x: width / 2 - 50,
        y: height - (yController++ * 2) * fontSize,
        size: 24,
        font: timesRomanFont,
    })
    //@ts-ignore
    page.drawText(`Date: ${new Date(invoice.createdAt).toLocaleString()}`, {
        x: 30,
        y: height - (yController++ * 2) * fontSize,
        size: fontSize,
        font: timesRomanFont,
    })
    //@ts-ignore
    page.drawText(`Customer: ${invoice.personId.firstName} ${invoice.personId.lastName}`, {
        x: 30,
        y: height - (yController++ * 2) * fontSize,
        size: fontSize,
        font: timesRomanFont,
    })
    page.drawText(`Address: ${invoice.address}`, {
        x: 30,
        y: height - (yController++ * 2) * fontSize,
        size: fontSize,
        font: timesRomanFont,
    })
    page.drawText(`Items:`, {
        x: 30,
        y: height - (yController++ * 2) * fontSize,
        size: fontSize,
        font: timesRomanFont,
    })

    // eslint-disable-next-line
    invoice.items.map((item): void => {
        //@ts-ignore
        page.drawText(`Name: ${item.productId.name}`, {
            x: 60,
            y: height - (yController++ * 2) * fontSize,
            size: fontSize,
            font: timesRomanFont,
        })
        page.drawText(`Quantity: ${item.quantity}`, {
            x: 60,
            y: height - (yController++ * 2) * fontSize,
            size: fontSize,
            font: timesRomanFont,
        })
        page.drawText(`Price: ${item.price}`, {
            x: 60,
            y: height - (yController++ * 2) * fontSize,
            size: fontSize,
            font: timesRomanFont,
        })
        page.drawLine({
            start: {x: 60, y: height - (yController * 2) * fontSize,},
            end: {x: width - 60, y: height - (yController++ * 2) * fontSize,},
            thickness: 2,
        })
    })

    const pdfBytes = await pdfDoc.save()

    const bytes = new Uint8Array(pdfBytes);
    const blob = new Blob([bytes], {type: "application/pdf"});
    const docUrl = URL.createObjectURL(blob);

    return docUrl
}