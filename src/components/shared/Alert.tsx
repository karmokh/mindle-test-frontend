import React, {useEffect} from "react";

export default function Alert({type, message, hide}: { type: string, message?: string | string[], hide: () => void }) {
    useEffect(() => {
        setTimeout(() => hide(), 5000)
    }, []);

    return <div className={"absolute right-4 bottom-10"}>
        <div className={`bg-${type} rounded-md flex flex-row text-background px-8 py-3 timer`}>
            <div>
                {typeof message === "object"
                    ? message.map((m, index) => {
                        return <div key={index}> - {m}</div>;
                    })
                    : message}
            </div>
        </div>
    </div>
}
