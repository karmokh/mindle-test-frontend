import PageLayout from "../../../components/shared/PageLayout";
import {Formik} from "formik";
import TextInput from "../../../components/inputs/TextInput";
import {useTranslation} from "react-i18next";
import PageTitle from "../../../components/shared/PageTitle";
import {useContext, useEffect, useState} from "react";
import {createInvoiceService, findOneInvoiceService, updateInvoiceService} from "../services/invoice.service";
import {IInvoice} from "../models/invoice.model";
import {useNavigate, useParams} from "react-router-dom";
import {Button, Select, SelectItem, Spinner} from "@nextui-org/react";
import {Plus, Trash} from "react-feather";
import {v1 as uuidv1} from 'uuid';
import {AppContext} from "../../../App";
import {findAllPersonsService} from "../../person/services/person.service";
import {findAllProductsService} from "../../product/services/product.service";
import {IPerson} from "../../person/models/person.model";
import {IProduct} from "../../product/models/product.model";

const InvoiceCreate = () => {
    const {id} = useParams();
    const [dataFetched, setDataFetched] = useState(!id)
    const [initialValues, setInitialValues] = useState<IInvoice>({
        personId: "",
        address: "",
        items: []
    })

    const [persons, setPerons] = useState<IPerson[]>([])
    const [products, setProducts] = useState<IProduct[]>([])

    //@ts-ignore
    const {appContext, setAppContext} = useContext(AppContext)

    const setAlert = ({show, message, type}: { show: boolean, message: string, type: string }) => {
        setAppContext({...appContext, alert: {show, message, type}});
    }

    const errorHandler = (err: { message: string }) => {
        setAlert({
            show: true,
            message: err.message,
            type: 'danger'
        })
    }

    const {t} = useTranslation();
    const navigate = useNavigate()
    const form = {
        title: `${id ? t('edit') : t('create')} ${t('invoice')}`,
        backRoute: '/invoices',
    }

    if (id && !dataFetched) {
        findOneInvoiceService({
            id: id,
            cb: (res: any) => {
                setInitialValues({
                    personId: res.personId,
                    address: res.address,
                    items: res.items
                })
                setDataFetched(true)
            },
            errorHandler: (err) => {
                errorHandler(err)
                setDataFetched(true)
            }
        })
    }

    useEffect(() => {
        findAllPersonsService({
            cb: (res) => {
                setPerons(res.data)
            },
            errorHandler: errorHandler
        })

        findAllProductsService({
            cb: (res) => {
                setProducts(res.data)
            },
            errorHandler: errorHandler
        })
    // eslint-disable-next-line
    }, []);

    return dataFetched ? <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
            if (id) {
                updateInvoiceService(id, values, ((res: any) => {
                    navigate(form.backRoute)
                }), errorHandler)
            } else {
                createInvoiceService(values, ((res: any) => {
                    navigate(form.backRoute)
                }), errorHandler)
            }
        }}
    >
        {
            props => (
                <PageLayout
                    titleContent={<PageTitle title={form.title} backRoute={form.backRoute}/>}
                    actionContent={
                        <div className={"grid grid-cols-2 gap-4 items-center justify-items-end"}>
                            <Button color={"primary"} onClick={() => props.handleSubmit()}>{t('submit')}</Button>
                        </div>
                    }
                >
                    <form onSubmit={props.handleSubmit} className={"mt-8 xl:mx-24 md-mx-8 mx-2"}>
                        <div className={"grid md:grid-cols-3 grid-cols-1 gap-4 md:gap-8 lg:gap-16"}>
                            <Select
                                size={"sm"}
                                label={`${t('select')} ${t('person')}`}
                                variant={"bordered"}
                                className="max-w-xs mt-3"
                                onChange={(e) => props.setFieldValue('personId', e.target.value)}
                                value={props.values.personId}
                            >
                                {persons.map((person: IPerson, index) => (
                                    <SelectItem
                                        key={person._id ?? index}
                                        value={person._id}>
                                        {`${person.firstName} ${person.lastName}`}
                                    </SelectItem>
                                ))}
                            </Select>
                            <Select
                                size={"sm"}
                                label={`${t('select')} ${t('address')}`}
                                variant={"bordered"}
                                className="max-w-xs mt-3"
                                onChange={(e) => props.setFieldValue('address', e.target.value)}
                                value={props.values.address}
                            >
                                {/*{*/}
                                {/*    //@ts-ignore*/}
                                {/*    persons.length && persons.find(person => person._id === props.values.personId)?.addresses?.map((address) => (*/}
                                {/*        <SelectItem*/}
                                {/*            key={address.address}*/}
                                {/*            value={address.address}>*/}
                                {/*            {address.title}*/}
                                {/*        </SelectItem>*/}
                                {/*    ))}*/}
                            </Select>
                        </div>

                        <div className={"mt-4"}>
                            <div className={"flex flex-row items-center"}>
                                <h1>{t('products')}</h1>
                                <Button className={"ml-4 w-1"} color={"primary"} size={"sm"}
                                        onClick={() => props.setFieldValue("items", [...props.values.items, {
                                            id: uuidv1(),
                                            productId: "",
                                            quantity: "",
                                            price: "",
                                        }])}>
                                    <Plus/>
                                </Button>
                            </div>
                            {
                                props.values.items.map((product: {
                                    id: string,
                                    productId: string,
                                    quantity: string,
                                    price: string,
                                }) => {
                                    return <div
                                        className={"border border-1 border-dashed rounded-md mt-4 p-4 max-h-[300px] flex flex-row items-center"}>
                                        <Select
                                            size={"sm"}
                                            label={`${t('select')} ${t('product')}`}
                                            variant={"bordered"}
                                            className="max-w-xs"
                                            onChange={(e) => props.setFieldValue('items', props.values.items.map(prd => {
                                                if (prd.id === product.id) {
                                                    let productData: IProduct | undefined = products.find(p => p._id === e.target.value)
                                                    if (productData) {
                                                        prd.productId = e.target.value
                                                        prd.price = productData.price
                                                    }
                                                }
                                                return prd
                                            }))}
                                            value={product.productId}
                                        >
                                            {products.map((product: IProduct, index) => (
                                                <SelectItem
                                                    key={product._id ?? index}
                                                    value={product._id}>
                                                    {product.name}
                                                </SelectItem>
                                            ))}
                                        </Select>
                                        <TextInput
                                            name="quantity"
                                            type={"number"}
                                            label={t('quantity')}
                                            value={product.quantity}
                                            onChange={(e) => props.setFieldValue('items', props.values.items.map(prd => {
                                                if (prd.id === product.id) prd.quantity = e.target.value
                                                return prd
                                            }))}
                                            onClear={() => props.setFieldValue('items', props.values.items.map(prd => {
                                                if (prd.id === product.id) prd.quantity = ""
                                                return prd
                                            }))}
                                            className={"mx-2"}
                                        />
                                        <TextInput
                                            name="price"
                                            type={"number"}
                                            label={t('price')}
                                            value={product.price}
                                            onChange={(e) => props.setFieldValue('items', props.values.items.map(prd => {
                                                if (prd.id === product.id) prd.price = e.target.value
                                                return prd
                                            }))}
                                            onClear={() => props.setFieldValue('items', props.values.items.map(prd => {
                                                if (prd.id === product.id) prd.price = ""
                                                return prd
                                            }))}
                                            className={"mx-2"}
                                        />
                                        <Trash size={48} className={"mx-2 text-danger cursor-pointer"} onClick={() => {
                                            props.setFieldValue('items', props.values.items.filter(prd => prd.id !== product.id))
                                        }}/>
                                    </div>
                                })
                            }
                        </div>
                    </form>
                </PageLayout>
            )
        }
    </Formik> : <Spinner/>
}

export default InvoiceCreate