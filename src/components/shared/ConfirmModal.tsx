import {Button, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader} from "@nextui-org/react";
import React, {useContext} from "react";
import {AppContext} from "../../App";
import {useTranslation} from "react-i18next";

const ConfirmModal = () => {
    //@ts-ignore
    const {appContext, setAppContext} = useContext(AppContext)
    const {t, i18n} = useTranslation();

    return <Modal
        isOpen={appContext.modal.show}
        backdrop={"blur"}
        placement={"top"}
        motionProps={{
            variants: {
                enter: {
                    y: 0,
                    opacity: 1,
                    transition: {
                        duration: 0.3,
                        ease: "easeOut",
                    },
                },
                exit: {
                    y: -20,
                    opacity: 0,
                    transition: {
                        duration: 0.2,
                        ease: "easeIn",
                    },
                },
            }
        }}
    >
        <ModalContent>
            {(onClose) => (
                <>
                    <ModalHeader className="flex flex-col gap-1"></ModalHeader>
                    <ModalBody>
                        <p>{t('do-you-accept-operation')}</p>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" variant="light" onPress={() => {
                            setAppContext({
                                ...appContext,
                                modal: {
                                    show: false,
                                    callback: () => {
                                    }
                                }
                            })
                        }}>{t('cancel')}</Button>
                        <Button color="danger" onClick={() => {
                            appContext.modal.callback()
                            setAppContext({
                                ...appContext,
                                modal: {
                                    show: false,
                                    callback: () => {
                                    }
                                }
                            })
                        }}>{t('delete')}</Button>
                    </ModalFooter>
                </>
            )}
        </ModalContent>
    </Modal>
}

export default ConfirmModal