export const limitPerPageValues = [5, 10, 15, 20, 25, 50, 100]

export function capitalize(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1);

}