import {useNavigate} from "react-router-dom";
import {ArrowLeft} from "react-feather";
import {useTranslation} from "react-i18next";

const PageTitle = ({title, backRoute}: {
    title: string,
    backRoute?: string
}) => {
    const {t, i18n} = useTranslation();
    const navigate = useNavigate()

    return (
        <div className={"flex flex-row gap-2 items-center"}>
            {backRoute && <ArrowLeft size={24} className={"cursor-pointer"} onClick={() => navigate(backRoute)}/>}
            <h1 className={"text-lg font-bold"}>{title}</h1>
        </div>
    )
}

export default PageTitle