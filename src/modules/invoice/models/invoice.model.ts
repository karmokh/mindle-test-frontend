export interface IInvoice {
    personId: string;
    address: string;
    items: { id: string, productId: string, quantity: string, price: string }[];
}