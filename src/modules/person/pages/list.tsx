import PageLayout from "../../../components/shared/PageLayout";
import PageTitle from "../../../components/shared/PageTitle";
import {useTranslation} from "react-i18next";
import {useNavigate} from "react-router-dom";
import React, {useContext, useState} from "react";
import {Button, Dropdown, DropdownItem, DropdownMenu, DropdownTrigger} from "@nextui-org/react";
import {deleteMultiplePersonsService, deletePersonService, findAllPersonsService} from "../services/person.service";
import TableComponent from "../../../components/Table";
import {MoreVertical, Plus} from "react-feather";
import {TextColumn} from "../../../components/Table/components";
import {IColumn, IFindAllServiceParams} from "../../../types/request-types";
import {AppContext, PaginateContext} from "../../../App";

const PersonList = () => {
    const navigate = useNavigate()
    const {t} = useTranslation();
    const [data, setData] = useState([])
    const [count, setCount] = useState(1)
    //@ts-ignore
    const {appContext, setAppContext} = useContext(AppContext)
    //@ts-ignore
    const {paginateContext} = useContext(PaginateContext)

    const setAlert = ({show, message, type}: { show: boolean, message: string, type: string }) => {
        setAppContext({...appContext, alert: {show, message, type}});
    }

    const errorHandler = (err: { message: string | string[] }) => {
        setAlert({
            show: true,
            // @ts-ignore
            message: err.message,
            type: 'danger'
        })
    }

    // Person Model Data
    const title = `${t('person')} ${t('list')}`
    const modelName = t('person')
    const addRoute = '/persons/add'
    const columns: IColumn[] = [
        {
            name: t('first-name'),
            uid: "firstName",
            render: (person: any, cellValue: string) => <TextColumn value={cellValue}/>
        },
        {
            name: t("last-name"),
            uid: "lastName",
            render: (person: any, cellValue: string) => <TextColumn value={cellValue}/>
        },
        {
            name: t("actions"),
            uid: "actions",
            render: (person: any) => <div
                className={`relative flex items-center gap-2`}>
                <Dropdown className="border-1 border-default-200" size={"sm"}>
                    <DropdownTrigger>
                        <Button isIconOnly radius="full" size="sm" variant="light">
                            <MoreVertical width={24} height={24}/>
                        </Button>
                    </DropdownTrigger>
                    <DropdownMenu variant={"flat"} color={"secondary"}>
                        <DropdownItem onClick={() => navigate('/persons/' + person._id)}>{t("edit")}</DropdownItem>
                        <DropdownItem onClick={() => {
                            setAppContext({
                                ...appContext,
                                modal: {
                                    show: true,
                                    callback: () => {
                                        deleteItems(person._id)
                                    },
                                }
                            })
                        }}>{t("delete")}</DropdownItem>
                    </DropdownMenu>
                </Dropdown>
            </div>
        },
    ]
    const getItems = ({page, limit, cb}: IFindAllServiceParams) => {
        findAllPersonsService({
            page, limit, cb: (res => {
                setData(res.data)
                // @ts-ignore
                setCount(res.total)
            }), errorHandler: errorHandler
        })
    }
    const deleteItems = (keys: string | string[]) => {
        if (Array.isArray(keys)) {
            deleteMultiplePersonsService({
                id: keys,
                cb: (res) => {
                    getItems(paginateContext)
                    setAlert({
                        show: true,
                        message: t("persons.deleted"),
                        type: 'danger'
                    })
                },
                errorHandler: errorHandler
            })
        } else {
            deletePersonService({
                id: keys,
                cb: (res) => {
                    getItems(paginateContext)
                    setAlert({
                        show: true,
                        message: t("person.deleted"),
                        type: 'danger'
                    })
                },
                errorHandler: errorHandler
            })
        }
    }

    return <PageLayout
        titleContent={<PageTitle title={title}/>}
        actionContent={
            <Button
                color={"primary"}
                endContent={<Plus/>}
                onClick={() => navigate(addRoute)}
                size="sm"
            >
                {t('add')}
            </Button>
        }
    >
        <div className="mt-8">
            <TableComponent
                modelName={modelName}
                items={data}
                count={count}
                columns={columns}
                getItems={getItems}
                deleteItems={deleteItems}
            />
        </div>
    </PageLayout>
}

export default PersonList;