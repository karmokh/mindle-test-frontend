import {IPerson} from "../models/person.model";
import {AxiosError, AxiosResponse} from "axios";
import {IFindAllServiceParams} from "../../../types/request-types";
import axios from "../../../axios/axios";

export function findAllPersonsService({page, limit, cb, errorHandler}: IFindAllServiceParams) {
    axios.get('/persons',
        {
            params: {
                page,
                limit,
            }
        }
    )
        .then(response => {
            cb && cb(response.data)
        })
        .catch(err => {
            errorHandler && errorHandler(err.response.data)
        })
}

export function findOnePersonService({id, cb, errorHandler}: {
    id: string,
    cb: (res: AxiosResponse) => void,
    errorHandler: (err: AxiosError) => void
}) {
    axios.get(`/persons/${id}`)
        .then(response => {
            cb(response.data)
        })
        .catch(err => {
            errorHandler(err.response.data)
        })
}

export function createPersonService(values: IPerson, cb: (res: AxiosResponse) => void, errorHandler: (err: AxiosError) => void) {
    axios.post('/persons', values)
        .then(response => {
            cb(response.data)
        })
        .catch(err => {
            errorHandler(err.response.data)
        })
}

export function updatePersonService(id: string, values: IPerson, cb: (res: AxiosResponse) => void, errorHandler: (err: AxiosError) => void) {
    axios.patch(`/persons/${id}`, values)
        .then(response => {
            cb(response.data)
        })
        .catch(err => {
            errorHandler(err.response.data)
        })
}

export function deletePersonService({id, cb, errorHandler}: {
    id: string,
    cb: (res: AxiosResponse) => void,
    errorHandler: (err: AxiosError) => void
}) {
    axios.delete(`/persons/${id}`)
        .then(response => {
            cb(response.data)
        })
        .catch(err => {
            errorHandler(err.response.data)
        })
}

export function deleteMultiplePersonsService({id, cb, errorHandler}: {
    id: string[],
    cb: (res: AxiosResponse) => void,
    errorHandler: (err: AxiosError) => void
}) {
    axios.delete('/persons/multiple', {
        data: id,
    })
        .then(response => {
            cb(response.data)
        })
        .catch(err => {
            errorHandler(err.response.data)
        })
}