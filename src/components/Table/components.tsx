import React from "react";

export const TextColumn = ({value}: { value: string }) => {
    return <p>{value}</p>
}