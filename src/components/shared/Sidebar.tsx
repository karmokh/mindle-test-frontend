/* eslint-disable react-hooks/exhaustive-deps */
import React, {memo} from "react";
import {Accordion, AccordionItem, Link} from "@nextui-org/react";
import {useNavigate} from "react-router-dom";
import {useTranslation} from "react-i18next";
import {ChevronRight} from "react-feather";

const sidebarItems = [
    {
        titleKey: "person",
        childs: [
            {
                titleKey: "create",
                url: "/persons/add",
            },
            {
                titleKey: "list",
                url: "/persons",
            },
        ],
    },
    {
        titleKey: "product",
        childs: [
            {
                titleKey: "create",
                url: "/products/add",
            },
            {
                titleKey: "list",
                url: "/products",
            },
        ],
    },
    {
        titleKey: "invoice",
        childs: [
            {
                titleKey: "create",
                url: "/invoices/add",
            },
            {
                titleKey: "list",
                url: "/invoices",
            },
        ],
    },
]

function Sidebar() {
    const navigate = useNavigate()
    const {t, i18n} = useTranslation();

    return (
        <div className={"bg-default-background"} style={{
            width: '12rem',
            position: "fixed",
            top: 50,
            left: 0,
            bottom: 0
        }}
        >
            <Accordion isCompact>
                {sidebarItems.map((item: {
                    titleKey: string,
                    childs: { titleKey: string, url: string }[]
                }, index: number) => {
                    return <AccordionItem key={index} aria-label={item.titleKey} title={t(item.titleKey)}
                                          indicator={<ChevronRight size={16}/>}
                                          style={{fontSize: "10px !important"}}>
                        {
                            item.childs && item.childs.map((child, ind: number) => {
                                return <div key={ind}>
                                    <Link className="pt-2 px-4" href={child.url} size={"sm"} color={"primary"}
                                          onClick={(e) => {
                                              e.preventDefault()
                                              navigate(child.url)
                                          }}>{t(child.titleKey)}</Link>
                                </div>
                            })
                        }
                    </AccordionItem>
                })}
            </Accordion>
        </div>
    );
}

export default memo(Sidebar);
