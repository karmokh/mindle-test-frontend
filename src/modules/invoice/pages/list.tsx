import PageLayout from "../../../components/shared/PageLayout";
import PageTitle from "../../../components/shared/PageTitle";
import {useTranslation} from "react-i18next";
import {useNavigate} from "react-router-dom";
import React, {useContext, useState} from "react";
import {
    Button,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownTrigger,
    Modal,
    ModalBody,
    ModalContent
} from "@nextui-org/react";
import {deleteInvoiceService, deleteMultipleInvoicesService, findAllInvoicesService} from "../services/invoice.service";
import TableComponent from "../../../components/Table";
import {MoreVertical, Plus} from "react-feather";
import {TextColumn} from "../../../components/Table/components";
import {IColumn, IFindAllServiceParams} from "../../../types/request-types";
import {AppContext, PaginateContext} from "../../../App";
import {invoicePdfGenerate} from "../../../utils/pdfGenerate";

const InvoiceList = () => {
    const navigate = useNavigate()
    const {t} = useTranslation();
    const [data, setData] = useState([])
    const [count, setCount] = useState(1)
    //@ts-ignore
    const {appContext, setAppContext} = useContext(AppContext)
    //@ts-ignore
    const {paginateContext} = useContext(PaginateContext)

    const [pdfData, setPdfData] = useState("");
    const [pdfModal, setPdfModal] = useState(false);

    const generatePdf = async (invoice: any, download = false) => {
        const pdfContent = await invoicePdfGenerate(invoice)

        if (download) {
            const link = document.createElement('a');
            link.href = pdfContent;
            link.download = '<Invoice-Pdf>';
            link.click();
        } else {
            setPdfData(pdfContent)
        }
    };

    const setAlert = ({show, message, type}: { show: boolean, message: string, type: string }) => {
        setAppContext({...appContext, alert: {show, message, type}});
    }

    const errorHandler = (err: { message: string }) => {
        setAlert({
            show: true,
            message: err.message,
            type: 'danger'
        })
    }

    // Invoice Model Data
    const title = `${t('invoice')} ${t('list')}`
    const modelName = t('invoice')
    const addRoute = '/invoices/add'
    const columns: IColumn[] = [
        {
            name: t('person'),
            uid: "person",
            render: (invoice: any, cellValue: string) => {
                return <TextColumn value={`${invoice.personId.firstName} ${invoice.personId.lastName}`}/>
            }
        },
        {
            name: t("address"),
            uid: "address",
            render: (invoice: any, cellValue: string) => <TextColumn value={cellValue}/>
        },
        {
            name: t('price'),
            uid: "price",
            render: (invoice: any, cellValue: string) => {
                return <TextColumn value={invoice.items.reduce((acc: number, cur: {
                    quantity: number,
                    price: number
                }) => acc + (cur.quantity * cur.price), 0)}/>
            }
        },
        {
            name: t("actions"),
            uid: "actions",
            render: (invoice: any) => <div
                className={`relative flex items-center gap-2`}>
                <Dropdown className="border-1 border-default-200" size={"sm"}>
                    <DropdownTrigger>
                        <Button isIconOnly radius="full" size="sm" variant="light">
                            <MoreVertical width={24} height={24}/>
                        </Button>
                    </DropdownTrigger>
                    <DropdownMenu variant={"flat"} color={"secondary"}>
                        <DropdownItem onClick={() => navigate('/invoices/' + invoice._id)}>{t("edit")}</DropdownItem>
                        <DropdownItem onClick={() => {
                            setPdfModal(true)
                            generatePdf(invoice)
                        }}>{t("view")}</DropdownItem>
                        <DropdownItem onClick={() => {
                            generatePdf(invoice, true)
                        }}>{t("pdf")}</DropdownItem>
                        <DropdownItem onClick={() => {
                            setAppContext({
                                ...appContext,
                                modal: {
                                    show: true,
                                    callback: () => {
                                        deleteItems(invoice._id)
                                    },
                                }
                            })
                        }}>{t("delete")}</DropdownItem>
                    </DropdownMenu>
                </Dropdown>
            </div>
        },
    ]
    const getItems = ({page, limit, cb}: IFindAllServiceParams) => {
        findAllInvoicesService({
            page, limit, cb: (res => {
                setData(res.data)
                // @ts-ignore
                setCount(res.total)
            }),
            errorHandler: errorHandler
        })
    }
    const deleteItems = (keys: string | string[], cb?: () => void) => {
        if (Array.isArray(keys)) {
            deleteMultipleInvoicesService({
                id: keys,
                cb: (res) => {
                    getItems(paginateContext)
                    setAlert({
                        show: true,
                        message: t("invoices.deleted"),
                        type: 'danger'
                    })
                },
                errorHandler: errorHandler
            })
        } else {
            deleteInvoiceService({
                id: keys,
                cb: (res) => {
                    getItems(paginateContext)
                    setAlert({
                        show: true,
                        message: t("invoice.deleted"),
                        type: 'danger'
                    })
                },
                errorHandler: errorHandler
            })
        }
    }

    return <PageLayout
        titleContent={<PageTitle title={title}/>}
        actionContent={
            <Button
                color={"primary"}
                endContent={<Plus/>}
                onClick={() => navigate(addRoute)}
                size="sm"
            >
                {t('add')}
            </Button>
        }
    >
        <div className="mt-8">
            <TableComponent
                modelName={modelName}
                items={data}
                count={count}
                columns={columns}
                getItems={getItems}
                deleteItems={deleteItems}
            />
        </div>

        <Modal
            isOpen={pdfModal}
            size={"full"}
            onOpenChange={(val) => setPdfModal(val)}
            backdrop={"blur"}
            placement={"top"}
        >
            <ModalContent>
                {(onClose) => (
                    <>
                        <ModalBody>
                            <iframe title={"Invoice Pdf"} src={pdfData} style={{width: '100%', height: '100%'}}></iframe>
                        </ModalBody>
                    </>
                )}
            </ModalContent>
        </Modal>
    </PageLayout>
}

export default InvoiceList;