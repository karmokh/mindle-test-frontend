import PageLayout from "../../../components/shared/PageLayout";
import PageTitle from "../../../components/shared/PageTitle";
import {useTranslation} from "react-i18next";
import {useNavigate} from "react-router-dom";
import React, {useContext, useState} from "react";
import {Button, Dropdown, DropdownItem, DropdownMenu, DropdownTrigger} from "@nextui-org/react";
import {deleteMultipleProductsService, deleteProductService, findAllProductsService} from "../services/product.service";
import TableComponent from "../../../components/Table";
import {MoreVertical, Plus} from "react-feather";
import {TextColumn} from "../../../components/Table/components";
import {IColumn, IFindAllServiceParams} from "../../../types/request-types";
import {AppContext, PaginateContext} from "../../../App";

const ProductList = () => {
    const navigate = useNavigate()
    const {t} = useTranslation();
    const [data, setData] = useState([])
    const [count, setCount] = useState(1)
    //@ts-ignore
    const {appContext, setAppContext} = useContext(AppContext)
    //@ts-ignore
    const {paginateContext} = useContext(PaginateContext)

    const setAlert = ({show, message, type}: { show: boolean, message: string, type: string }) => {
        setAppContext({...appContext, alert: {show, message, type}});
    }

    const errorHandler = (err: { message: string }) => {
        setAlert({
            show: true,
            message: err.message,
            type: 'danger'
        })
    }

    // Product Model Data
    const title = `${t('product')} ${t('list')}`
    const modelName = t('product')
    const addRoute = '/products/add'
    const columns: IColumn[] = [
        {
            name: t('name'),
            uid: "name",
            render: (product: any, cellValue: string) => <TextColumn value={cellValue}/>
        },
        {
            name: t("price"),
            uid: "price",
            render: (product: any, cellValue: string) => <TextColumn value={cellValue}/>
        },
        {
            name: t("actions"),
            uid: "actions",
            render: (product: any) => <div
                className={`relative flex items-center gap-2`}>
                <Dropdown className="border-1 border-default-200" size={"sm"}>
                    <DropdownTrigger>
                        <Button isIconOnly radius="full" size="sm" variant="light">
                            <MoreVertical width={24} height={24}/>
                        </Button>
                    </DropdownTrigger>
                    <DropdownMenu variant={"flat"} color={"secondary"}>
                        <DropdownItem onClick={() => navigate('/products/' + product._id)}>{t("edit")}</DropdownItem>
                        <DropdownItem onClick={() => {
                            setAppContext({
                                ...appContext,
                                modal: {
                                    show: true,
                                    callback: () => {
                                        deleteItems(product._id)
                                    },
                                }
                            })
                        }}>{t("delete")}</DropdownItem>
                    </DropdownMenu>
                </Dropdown>
            </div>
        },
    ]
    const getItems = ({page, limit, cb}: IFindAllServiceParams) => {
        findAllProductsService({
            page, limit, cb: (res => {
                setData(res.data)
                // @ts-ignore
                setCount(res.total)
            }),
            errorHandler: errorHandler
        })
    }
    const deleteItems = (keys: string | string[]) => {
        if (Array.isArray(keys)) {
            deleteMultipleProductsService({
                id: keys,
                cb: (res) => {
                    getItems(paginateContext)
                    setAlert({
                        show: true,
                        message: t("products.deleted"),
                        type: 'danger'
                    })
                },
                errorHandler: errorHandler
            })
        } else {
            deleteProductService({
                id: keys,
                cb: (res) => {
                    getItems(paginateContext)
                    setAlert({
                        show: true,
                        message: t("product.deleted"),
                        type: 'danger'
                    })
                },
                errorHandler: errorHandler
            })
        }
    }

    return <PageLayout
        titleContent={<PageTitle title={title}/>}
        actionContent={
            <Button
                color={"primary"}
                endContent={<Plus/>}
                onClick={() => navigate(addRoute)}
                size="sm"
            >
                {t('add')}
            </Button>
        }
    >
        <div className="mt-8">
            <TableComponent
                modelName={modelName}
                items={data}
                count={count}
                columns={columns}
                getItems={getItems}
                deleteItems={deleteItems}
            />
        </div>
    </PageLayout>
}

export default ProductList;