import {Route} from "react-router-dom";
import ProductCreate from "./pages/create";
import ProductList from "./pages/list";

const basePath = '/products'

export const ProductRoutes = [
    <Route path={`${basePath}`} element={<ProductList/>}/>,
    <Route path={`${basePath}/add`} element={<ProductCreate/>}/>,
    <Route path={`${basePath}/:id`} element={<ProductCreate/>}/>,
]