/** @type {import('tailwindcss').Config} */
const {nextui} = require("@nextui-org/react");

module.exports = {
    content: [
        "./src/**/*.{js,jsx,ts,tsx}",
        "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {
        screens: {
            sm: '480px',
            md: '768px',
            lg: '976px',
            xl: '1440px',
        },
        colors: {
            'blue': '#36bdbd',
            'purple': '#7e5bef',
            'pink': '#ff49db',
            'orange': '#ff7849',
            'green': '#13ce66',
            'yellow': '#ffc82c',
            'gray-dark': '#273444',
            'gray': '#abb1b9',
            'gray-light': '#d3dce6',
        },
        extend: {
            spacing: {
                '128': '32rem',
                '144': '36rem',
            },
            borderRadius: {
                '4xl': '2rem',
            }
        }
    },
    darkMode: "class",
    plugins: [
        nextui({
            themes: {
                light: {
                    colors: {
                        background: "#e9e9e9",
                        foreground: "#333",
                        default: {
                            background: "#fff",
                            foreground: "#999",
                            DEFAULT: "#777",
                        },
                        primary: {
                            foreground: "#FFFFFF",
                            DEFAULT: "#006FEE",
                        },
                    },
                },
                dark: {
                    colors: {
                        background: "#333",
                        foreground: "#e9e9e9",
                        default: {
                            background: "#333",
                            foreground: "#999",
                            DEFAULT: "#fff",
                        },
                        primary: {
                            foreground: "#FFFFFF",
                            DEFAULT: "#006FEE",
                        },
                    },
                },
            },
        }),
    ],
}

