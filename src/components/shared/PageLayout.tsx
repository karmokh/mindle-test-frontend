import {Divider} from "@nextui-org/react";
import {ReactNode} from "react";

const PageLayout = ({children, titleContent, actionContent}: {
    children: ReactNode,
    titleContent?: ReactNode,
    actionContent?: ReactNode
}) => {
    return <div className={"mt-10 mr-10 ml-60 p-4 pt-8 bg-default-background rounded-xl"}>
        <div className={"p-0 px-8"}>
            <div className="flex flex-row justify-between">
                {titleContent}
                {actionContent}
            </div>
            <Divider className={"my-4"}/>
            {children}
        </div>
    </div>
}

export default PageLayout;