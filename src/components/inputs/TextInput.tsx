import {Input} from "@nextui-org/react";
import {ChangeEventHandler, ReactNode} from "react";

const TextInput = ({
                       label,
                       placeholder = "",
                       className,
                       type = "text",
                       isClearable = true,
                       isRequired = false,
                       isReadOnly = false,
                       isDisabled = false,
                       startContent = null,
                       endContent = null,
                       style = {},
                       onChange,
                       onClear,
                       value,
                       name
                   }: {
    label: string,
    name?: string,
    value?: string,
    onChange?: ChangeEventHandler<HTMLInputElement>,
    onClear?: () => void,
    placeholder?: string,
    className?: string,
    type?: string,
    isClearable?: boolean,
    isRequired?: boolean,
    isReadOnly?: boolean,
    isDisabled?: boolean,
    startContent?: ReactNode | null
    endContent?: ReactNode | null,
    style?: {}
}) => {
    return (
        <Input
            name={name}
            value={value}
            onChange={onChange}
            isRequired={isRequired}
            isReadOnly={isReadOnly}
            isDisabled={isDisabled}
            color={"default"}
            isClearable={isClearable}
            classNames={{
                clearButton: ["left-auto right-2 bottom-2"],
                input: ["pr-2 pl-2"],
                label: ["text-sm"],
                inputWrapper: ["my-3 px-2 py-2 justify-start border-small border-default-foreground"],
            }}
            onClear={onClear}
            variant={"bordered"}
            labelPlacement="inside"
            className={className}
            type={type}
            label={label}
            style={style}
            placeholder={placeholder}
            radius={"sm"}
            size={"sm"}
            startContent={startContent}
            endContent={endContent}
        />
    )
}
export default TextInput;