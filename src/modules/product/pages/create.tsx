import PageLayout from "../../../components/shared/PageLayout";
import {Formik} from "formik";
import TextInput from "../../../components/inputs/TextInput";
import {useTranslation} from "react-i18next";
import PageTitle from "../../../components/shared/PageTitle";
import {useContext, useState} from "react";
import {createProductService, findOneProductService, updateProductService} from "../services/product.service";
import {IProduct} from "../models/product.model";
import {useNavigate, useParams} from "react-router-dom";
import {Button, Spinner} from "@nextui-org/react";
import {AppContext} from "../../../App";

const ProductCreate = () => {
    const {id} = useParams();
    const [dataFetched, setDataFetched] = useState(!id)
    const [initialValues, setInitialValues] = useState<IProduct>({
        name: "",
        price: "",
    })

    //@ts-ignore
    const {appContext, setAppContext} = useContext(AppContext)

    const setAlert = ({show, message, type}: { show: boolean, message: string, type: string }) => {
        setAppContext({...appContext, alert: {show, message, type}});
    }

    const errorHandler = (err: { message: string }) => {
        setAlert({
            show: true,
            message: err.message,
            type: 'danger'
        })
    }

    const {t} = useTranslation();
    const navigate = useNavigate()
    const form = {
        title: `${id ? t('edit') : t('create')} ${t('product')}`,
        backRoute: '/products',
    }

    if (id && !dataFetched) {
        findOneProductService({
            id: id,
            cb: (res: any) => {
                setInitialValues({
                    name: res.name,
                    price: res.price
                })
                setDataFetched(true)
            },
            errorHandler: (err) => {
                errorHandler(err)
                setDataFetched(true)
            }
        })
    }

    return dataFetched ? <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
            if (id) {
                updateProductService(id, values, ((res: any) => {
                    navigate(form.backRoute)
                }), errorHandler)
            } else {
                createProductService(values, ((res: any) => {
                    navigate(form.backRoute)
                }), errorHandler)
            }
        }}
    >
        {
            props => (
                <PageLayout
                    titleContent={<PageTitle title={form.title} backRoute={form.backRoute}/>}
                    actionContent={
                        <div className={"grid grid-cols-2 gap-4 items-center justify-items-end"}>
                            <Button color={"primary"} onClick={() => props.handleSubmit()}>{t('submit')}</Button>
                        </div>
                    }
                >
                    <form onSubmit={props.handleSubmit} className={"mt-8 xl:mx-24 md-mx-8 mx-2"}>
                        <div className={"grid md:grid-cols-3 grid-cols-1 gap-4 md:gap-8 lg:gap-16"}>
                            <TextInput
                                name="name"
                                label={t('name')}
                                value={props.values.name}
                                onChange={props.handleChange}
                                onClear={props.handleReset}
                            />
                            <TextInput
                                name="price"
                                type={"number"}
                                label={t('price')}
                                value={props.values.price}
                                onChange={props.handleChange}
                                onClear={props.handleReset}
                            />
                        </div>
                    </form>
                </PageLayout>
            )
        }
    </Formik> : <Spinner/>
}

export default ProductCreate