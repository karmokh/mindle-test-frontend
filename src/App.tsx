import React, {createContext, useState} from 'react';
import './App.css';
import Router from "./routes";
import Header from "./components/shared/Header";
import Sidebar from "./components/shared/Sidebar";
import Alert from "./components/shared/Alert";
import ConfirmModal from "./components/shared/ConfirmModal";

export const AppContext = createContext({})
export const PaginateContext = createContext({})

function App() {
    const [theme, setTheme] = useState('light')
    const [appContext, setAppContext] = React.useState({
        alert: {
            show: false,
            message: "",
            type: ''
        },
        modal: {
            show: false,
            callback: () => {
            },
        }
    });
    const [paginateContext, setPaginateContext] = React.useState({page: 1, limit: 10});

    return (
        <AppContext.Provider value={{appContext, setAppContext}}>
            <PaginateContext.Provider value={{paginateContext, setPaginateContext}}>
                <main className={`${theme} text-foreground bg-background`} style={{minHeight: '100vh'}}>
                    <Header setTheme={setTheme}/>
                    <Sidebar/>
                    <Router/>
                    <ConfirmModal/>
                    {appContext.alert.show && <Alert message={appContext.alert.message} type={appContext.alert.type}
                                                     hide={() => setAppContext({
                                                         ...appContext,
                                                         alert: {show: false, message: "", type: ""}
                                                     })}/>}
                </main>
            </PaginateContext.Provider>
        </AppContext.Provider>
    );
}

export default React.memo(App);
