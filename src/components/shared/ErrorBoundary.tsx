import React from "react";

class ErrorBoundary extends React.Component {
    constructor(props: {} | Readonly<{}>) {
        super(props);
        this.state = {hasError: false, reload: 0, showAlert: false};
    }

    static getDerivedStateFromError(error: any) {
        return {hasError: true};
    }

    componentDidCatch(error: any, errorInfo: any) {
        console.log("Error : =======================>  ", error);
        console.log("ErrorInfo : =======================>  ", errorInfo);
    }

    render() {
        // @ts-ignore
        if (this.state.hasError) {
            return (
                <div
                    style={{
                        display: "flex",
                        width: "100%",
                        height: "100vh",
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                >
                </div>
            );
        }
        // @ts-ignore
        return <>{this.props.children}</>;
    }
}

export default ErrorBoundary;
