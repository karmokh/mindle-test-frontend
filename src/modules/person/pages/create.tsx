import PageLayout from "../../../components/shared/PageLayout";
import {Formik} from "formik";
import TextInput from "../../../components/inputs/TextInput";
import {useTranslation} from "react-i18next";
import PageTitle from "../../../components/shared/PageTitle";
import {useContext, useState} from "react";
import {createPersonService, findOnePersonService, updatePersonService} from "../services/person.service";
import {IPerson} from "../models/person.model";
import {useNavigate, useParams} from "react-router-dom";
import {Button, Spinner} from "@nextui-org/react";
import {Plus, Trash} from "react-feather";
import {v1 as uuidv1} from 'uuid';
import {AppContext} from "../../../App";

const PersonCreate = () => {
    const {id} = useParams();
    const [dataFetched, setDataFetched] = useState(!id)
    const [initialValues, setInitialValues] = useState<IPerson>({
        firstName: "",
        lastName: "",
        addresses: []
    })

    const {t} = useTranslation();
    const navigate = useNavigate()
    const form = {
        title: `${id ? t('edit') : t('create')} ${t('person')}`,
        backRoute: '/persons',
    }

    //@ts-ignore
    const {appContext, setAppContext} = useContext(AppContext)

    const setAlert = ({show, message, type}: { show: boolean, message: string, type: string }) => {
        setAppContext({...appContext, alert: {show, message, type}});
    }

    const errorHandler = (err: { message: string }) => {
        setAlert({
            show: true,
            message: err.message,
            type: 'danger'
        })
    }

    if (id && !dataFetched) {
        findOnePersonService({
            id: id,
            cb: (res: any) => {
                setInitialValues({
                    firstName: res.firstName,
                    lastName: res.lastName,
                    addresses: res.addresses
                })
                setDataFetched(true)
            },
            errorHandler: (err) => {
                errorHandler(err)
                setDataFetched(true)
            }
        })
    }

    return dataFetched ? <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
            if (id) {
                updatePersonService(id, values, ((res: any) => {
                    navigate(form.backRoute)
                }), errorHandler)
            } else {
                createPersonService(values, ((res: any) => {
                    navigate(form.backRoute)
                }), errorHandler)
            }
        }}
    >
        {
            props => (
                <PageLayout
                    titleContent={<PageTitle title={form.title} backRoute={form.backRoute}/>}
                    actionContent={
                        <div className={"grid grid-cols-2 gap-4 items-center justify-items-end"}>
                            <Button color={"primary"}
                                    onClick={() => props.handleSubmit()}>{t('submit')}</Button>
                        </div>
                    }
                >
                    <form onSubmit={props.handleSubmit} className={"mt-8 xl:mx-24 md-mx-8 mx-2"}>
                        <div className={"grid md:grid-cols-3 grid-cols-1 gap-4 md:gap-8 lg:gap-16"}>
                            <TextInput
                                name="firstName"
                                label={t('first-name')}
                                value={props.values.firstName}
                                onChange={props.handleChange}
                                onClear={props.handleReset}
                            />
                            <TextInput
                                name="lastName"
                                label={t('last-name')}
                                value={props.values.lastName}
                                onChange={props.handleChange}
                                onClear={props.handleReset}
                            />
                        </div>

                        <div className={"mt-4"}>
                            <div className={"flex flex-row items-center"}>
                                <h1>{t('address')}</h1>
                                <Button className={"ml-4 w-1"} color={"primary"} size={"sm"}
                                        onClick={() => props.setFieldValue("addresses", [...props.values.addresses, {
                                            id: uuidv1(),
                                            title: "",
                                            address: "",
                                        }])}>
                                    <Plus/>
                                </Button>
                            </div>
                            {
                                props.values.addresses.map((address: {
                                    id: string,
                                    title: string,
                                    address: string
                                }) => {
                                    return <div
                                        className={"border border-1 border-dashed rounded-md mt-4 p-4 max-h-[300px] flex flex-row items-center"}>
                                        <TextInput
                                            name="title"
                                            label={t('title')}
                                            value={address.title}
                                            onChange={(e) => props.setFieldValue('addresses', props.values.addresses.map(addr => {
                                                if (addr.id === address.id) addr.title = e.target.value
                                                return addr
                                            }))}
                                            onClear={() => props.setFieldValue('addresses', props.values.addresses.map(addr => {
                                                if (addr.id === address.id) addr.title = ""
                                                return addr
                                            }))}
                                            className={"mx-2"}
                                        />
                                        <TextInput
                                            name="address"
                                            label={t('address')}
                                            value={address.address}
                                            onChange={(e) => props.setFieldValue('addresses', props.values.addresses.map(addr => {
                                                if (addr.id === address.id) addr.address = e.target.value
                                                return addr
                                            }))}
                                            onClear={() => props.setFieldValue('addresses', props.values.addresses.map(addr => {
                                                if (addr.id === address.id) addr.address = ""
                                                return addr
                                            }))}
                                            className={"mx-2"}
                                        />
                                        <Trash size={48} className={"mx-2 text-danger cursor-pointer"} onClick={() => {
                                            props.setFieldValue('addresses', props.values.addresses.filter(addr => addr.id !== address.id))
                                        }}/>
                                    </div>
                                })
                            }
                        </div>
                    </form>
                </PageLayout>
            )
        }
    </Formik> : <Spinner/>
}

export default PersonCreate