import axios from "axios";

const {REACT_APP_BACKEND_BASE_URL} = process.env;

export default axios.create({
    baseURL: REACT_APP_BACKEND_BASE_URL,
    timeout: 3000000,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Cache-Control": "no-cache",
        "Pragma": "no-cache",
    },
});
