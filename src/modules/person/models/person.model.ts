export interface IPerson {
    _id?: string;
    firstName: string;
    lastName: string;
    label?: string
    addresses: { id: string, title: string, address: string }[];
}