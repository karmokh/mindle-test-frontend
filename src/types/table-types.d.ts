import {IColumn, IFindAllServiceParams} from "./request-types";

export interface ITableParamsType {
    items: never[],
    count: number,
    columns: IColumn[],
    modelName: string,
    getItems: ({page, limit}: IFindAllServiceParams) => void,
    deleteItems: (keys: string | string[]) => void,
}