import {Route} from "react-router-dom";
import InvoiceCreate from "./pages/create";
import InvoiceList from "./pages/list";

const basePath = '/invoices'

export const InvoiceRoutes = [
    <Route path={`${basePath}`} element={<InvoiceList/>}/>,
    <Route path={`${basePath}/add`} element={<InvoiceCreate/>}/>,
    <Route path={`${basePath}/:id`} element={<InvoiceCreate/>}/>,
]