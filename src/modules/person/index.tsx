import {Route} from "react-router-dom";
import PersonCreate from "./pages/create";
import PersonList from "./pages/list";

const basePath = '/persons'

export const PersonRoutes = [
    <Route path={`${basePath}`} element={<PersonList/>}/>,
    <Route path={`${basePath}/add`} element={<PersonCreate/>}/>,
    <Route path={`${basePath}/:id`} element={<PersonCreate/>}/>,
]