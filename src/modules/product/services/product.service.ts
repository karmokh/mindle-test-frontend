import {IProduct} from "../models/product.model";
import {AxiosError, AxiosResponse} from "axios";
import {IFindAllServiceParams} from "../../../types/request-types";
import axios from "../../../axios/axios";

export function findAllProductsService({page, limit, cb, errorHandler}: IFindAllServiceParams) {
    axios.get('/products',
        {
            params: {
                page,
                limit,
            }
        }
    )
        .then(response => {
            cb && cb(response.data)
        })
        .catch(err => {
            errorHandler && errorHandler(err.response.data)
        })
}

export function findOneProductService({id, cb, errorHandler}: {
    id: string,
    cb: (res: AxiosResponse) => void,
    errorHandler: (err: AxiosError) => void
}) {
    axios.get(`/products/${id}`)
        .then(response => {
            cb(response.data)
        })
        .catch(err => {
            errorHandler(err.response.data)
        })
}

export function createProductService(values: IProduct, cb: (res: AxiosResponse) => void, errorHandler: (err: AxiosError) => void) {
    axios.post('/products', values)
        .then(response => {
            cb(response.data)
        })
        .catch(err => {
            errorHandler(err.response.data)
        })
}

export function updateProductService(id: string, values: IProduct, cb: (res: AxiosResponse) => void, errorHandler: (err: AxiosError) => void) {
    axios.patch(`/products/${id}`, values)
        .then(response => {
            cb(response.data)
        })
        .catch(err => {
            errorHandler(err.response.data)
        })
}

export function deleteProductService({id, cb, errorHandler}: {
    id: string,
    cb: (res: AxiosResponse) => void,
    errorHandler: (err: AxiosError) => void
}) {
    axios.delete(`/products/${id}`)
        .then(response => {
            cb(response.data)
        })
        .catch(err => {
            errorHandler(err.response.data)
        })
}

export function deleteMultipleProductsService({id, cb, errorHandler}: {
    id: string[],
    cb: (res: AxiosResponse) => void,
    errorHandler: (err: AxiosError) => void
}) {
    axios.delete('/products/multiple', {
        data: id
    })
        .then(response => {
            cb(response.data)
        })
        .catch(err => {
            errorHandler(err.response.data)
        })
}