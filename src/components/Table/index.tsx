import React, {useContext, useEffect} from "react";
import {
    Button,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownTrigger,
    Pagination,
    Select,
    SelectItem,
    Table,
    TableBody,
    TableCell,
    TableColumn,
    TableHeader,
    TableRow,
} from "@nextui-org/react";
import {useTranslation} from "react-i18next";
import {ChevronDown} from "react-feather";
import {ITableParamsType} from "../../types/table-types";
import {IColumn} from "../../types/request-types";
import {AppContext, PaginateContext} from "../../App";
import {capitalize, limitPerPageValues} from "./utils";

export default function TableComponent({
                                           items,
                                           count,
                                           columns,
                                           getItems,
                                           deleteItems,
                                           modelName
                                       }: ITableParamsType) {
    const {t, i18n} = useTranslation();
    //@ts-ignore
    const {appContext, setAppContext} = useContext(AppContext)
    //@ts-ignore
    const {paginateContext, setPaginateContext} = useContext(PaginateContext)

    const classNames = React.useMemo(
        () => ({
            wrapper: ["max-h-[382px]", "max-w-3xl d"],
            th: ["bg-transparent", "text-default-500", "border-b", "border-divider", "text-left"],
            td: [
                "group-data-[first=true]:first:before:rounded-none",
                "group-data-[first=true]:last:before:rounded-none",
                "group-data-[middle=true]:before:rounded-none",
                "group-data-[last=true]:first:before:rounded-none",
                "group-data-[last=true]:last:before:rounded-none",
            ],
        }),
        [],
    );

    const columnIds = columns.map((col: IColumn) => col.uid)
    const [visibleColumns, setVisibleColumns] = React.useState(new Set(columnIds));
    const [selectedKeys, setSelectedKeys] = React.useState(new Set([]));

    const pages = Math.ceil(count / paginateContext.limit);

    const headerColumns = React.useMemo(() => {
        return columns.filter((column: IColumn) => Array.from(visibleColumns).includes(column.uid));
    }, [visibleColumns]);

    const onLimitPerPageChange = React.useCallback((e: { target: { value: string }; }) => {
        console.log(e.target)
        const limit = Number(e.target.value)

        setPaginateContext({page: 1, limit: limit});

        getItems({
            page: 1,
            limit: limit,
        })
    }, [
        paginateContext,
    ]);

    const topContent = React.useMemo(() => {
        return (
            <div className="flex flex-col gap-4">
                <div className="flex justify-between gap-3 items-end">
                    <div className="flex gap-3">
                        <Dropdown>
                            <DropdownTrigger className="hidden sm:flex">
                                <Button
                                    endContent={<ChevronDown className="text-small"/>}
                                    size="sm"
                                    color={"secondary"}
                                    variant="flat"
                                >
                                    {t('columns')}
                                </Button>
                            </DropdownTrigger>
                            <DropdownMenu
                                disallowEmptySelection
                                closeOnSelect={false}
                                selectionMode="multiple"
                                color={"secondary"}
                                variant={"flat"}
                                defaultSelectedKeys={visibleColumns}
                                //@ts-ignore
                                onSelectionChange={setVisibleColumns}
                            >
                                {columns.map((column: IColumn) => (
                                    <DropdownItem key={column.uid} className="capitalize">
                                        {capitalize(column.name)}
                                    </DropdownItem>
                                ))}
                            </DropdownMenu>
                        </Dropdown>
                        {
                            ((typeof (selectedKeys) === 'string' && selectedKeys === "all") || selectedKeys.size > 0) &&
                            <Button
                                size="sm"
                                color={"danger"}
                                variant="solid"
                                onPress={() => {
                                    setAppContext({
                                        ...appContext,
                                        modal: {
                                            show: true,
                                            callback: () => {
                                                deleteItems(Array.from(selectedKeys))
                                            },
                                        }
                                    })
                                }}
                            >
                                {t('delete')}
                            </Button>
                        }
                    </div>
                </div>
                <div className="flex justify-between items-center">
                    <span className="text-default-400 text-small">{count} {modelName}</span>
                    <div className="flex text-default-400 text-small">
                        <Select
                            variant={"flat"}
                            className="w-20"
                            size={"sm"}
                            value={paginateContext.limit}
                            onChange={onLimitPerPageChange}
                        >
                            {limitPerPageValues.map((page: number) => (
                                <SelectItem key={page} value={page}>{page}</SelectItem>
                            ))}
                        </Select>
                    </div>
                </div>
            </div>
        );
    }, [
        visibleColumns,
        onLimitPerPageChange,
        selectedKeys,
        paginateContext,
        count
    ]);

    const bottomContent = React.useMemo(() => {
        return (
            <div className="py-2 px-2 flex justify-between items-center">
                <Pagination
                    showControls
                    color="primary"
                    page={paginateContext.page}
                    total={pages}
                    variant="light"
                    onChange={(page: number) => {
                        console.log(page)
                        setPaginateContext({...paginateContext, page: page});
                        getItems({
                            page: page,
                            limit: paginateContext.limit,
                        })
                    }}
                />
                <span className="text-small text-default-400">{`${selectedKeys.size} ${t('selected')}`}</span>
            </div>
        );
    }, [
        selectedKeys,
        pages,
        paginateContext,
        count
    ]);

    useEffect(() => {
        getItems({
            page: paginateContext.page,
            limit: paginateContext.limit,
        })
    }, [])

    return (
        <div>
            <Table
                isCompact
                removeWrapper
                color={"primary"}
                classNames={classNames}
                topContent={topContent}
                topContentPlacement="outside"
                bottomContent={bottomContent}
                bottomContentPlacement="inside"
                selectedKeys={selectedKeys}
                selectionMode="multiple"
                //@ts-ignore
                onSelectionChange={setSelectedKeys}
            >
                <TableHeader columns={headerColumns}>
                    {(column: IColumn) => (
                        <TableColumn key={column.uid}>
                            <span className={"ml-2"}>{column.name}</span>
                        </TableColumn>
                    )}
                </TableHeader>
                <TableBody emptyContent={t('no-data-to-display')} items={items}>
                    {(item: any) => (
                        <TableRow key={item._id}>
                            {(columnKey) =>
                                <TableCell>{columns.find((col: IColumn) => col.uid == columnKey)?.render(item, item[columnKey])}</TableCell>}
                        </TableRow>
                    )}
                </TableBody>
            </Table>
        </div>
    );
}
